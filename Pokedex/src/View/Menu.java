package View;

import Controller.*;
import Model.*;
import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;	
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.EventQueue;


public class Menu extends JFrame{

	private JFrame frmPokedex;
	private JTextField textFieldTreinador;
	private JPanel panelMenu;
	private JPanel panelCadrastoTreinador;
	private JPanel panelListPokemon;
	private JTextField textFieldPokemonsTreinador;
	private JTextField textFieldNomePokemon;
	private JTextField textFieldTipoPokemon;
	public ArrayList <Pokemon> pokemon = new ArrayList<Pokemon>();
	ArrayList <Treinador> treinador = new ArrayList<Treinador>();
	int nTreinador = 0;
	private JTextField textFieldTreinadorPesquisa;
	private JTextField textFieldPokemonPesquisa;

	
public static void main(String[] args) {
	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frmPokedex.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu() {
		{
			frmPokedex = new JFrame();
			frmPokedex.setTitle("Pokedex Menu");
			frmPokedex.setBounds(400, 400, 750, 500);
			frmPokedex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frmPokedex.getContentPane().setLayout(new CardLayout(0, 0));
			
			String [][]bancoPokemons_1 = new String[721][20]; String [][]bancoPokemons_2 = new String[722][20];
			bancoPokemons_1= BancoDeDados.Read("../data/csv_files/POKEMONS_DATA_1.csv");
			bancoPokemons_2 = BancoDeDados.Read("../data/csv_files/POKEMONS_DATA_2.csv");
			String [][]bancoPokemons = new String[723][40];                                 
			bancoPokemons = BancoDeDados.concatenaArquivos(bancoPokemons_1, bancoPokemons_2);
			pokemon = BancoDeDados.sets(bancoPokemons);
			
			
			
			
			//Menu
			JPanel panelMenu = new JPanel();
			panelMenu.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelMenu, "name_3254099382482");
			panelMenu.setLayout(null);
			
			JPanel panelListPokemon = new JPanel();
			panelListPokemon.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelListPokemon, "name_627469403799");
			panelListPokemon.setLayout(null);
			
			JLabel lblPesquisaPokemon = new JLabel("Pesquisa Pokemon");
			lblPesquisaPokemon.setForeground(Color.WHITE);
			lblPesquisaPokemon.setBounds(277, 31, 207, 15);
			panelListPokemon.add(lblPesquisaPokemon);
			
			JPanel panel_2 = new JPanel();
			panel_2.setBackground(Color.BLACK);
			panel_2.setBounds(103, 101, 510, 260);
			panelListPokemon.add(panel_2);
			panel_2.setLayout(null);
			
			JPanel panelPesquisaTipo = new JPanel();
			panelPesquisaTipo.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelPesquisaTipo, "name_6399692364202");
			panelPesquisaTipo.setLayout(null);
			
			JLabel lblPesquisaTipoPokemon = new JLabel("Pesquisa Tipo Pokemon");
			lblPesquisaTipoPokemon.setForeground(Color.WHITE);
			lblPesquisaTipoPokemon.setBounds(266, 12, 190, 15);
			panelPesquisaTipo.add(lblPesquisaTipoPokemon);
			
			JPanel panel_3 = new JPanel();
			panel_3.setBackground(Color.DARK_GRAY);
			panel_3.setLayout(null);
			panel_3.setBounds(132, 39, 448, 51);
			panelPesquisaTipo.add(panel_3);
			
			JLabel label = new JLabel("Insira:");
			label.setForeground(Color.WHITE);
			label.setBounds(12, 14, 66, 15);
			panel_3.add(label);
			
			textFieldTipoPokemon = new JTextField();
			textFieldTipoPokemon.setColumns(10);
			textFieldTipoPokemon.setBounds(64, 12, 227, 19);
			panel_3.add(textFieldTipoPokemon);
			
			JScrollPane scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(169, 102, 366, 342);
			panelPesquisaTipo.add(scrollPane_2);
			
			JList listTipoPokemon = new JList();
			scrollPane_2.setViewportView(listTipoPokemon);
			
			JButton btnPesquisarTipoPokemon = new JButton("Pesquisar");
			btnPesquisarTipoPokemon.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					
					
					String campo = textFieldTipoPokemon.getText();
					DefaultListModel dlm = new DefaultListModel();
					boolean var = false;
					
					
					if(campo.isEmpty())
					{
						JOptionPane.showMessageDialog(btnPesquisarTipoPokemon, "Entrada Invalida!", "Aviso", 0);
					}
					
					for(Pokemon p : pokemon)
					{
				
						if(campo.equalsIgnoreCase(p.getType1()) || campo.equalsIgnoreCase(p.getType2()))
						{
							dlm.addElement("Name: " + p.getName());
							dlm.addElement("Typo 1: " + p.getType1());
							dlm.addElement("Typo 2: " + p.getType2());
							dlm.addElement("Total: " + p.getTotal());
							dlm.addElement("HP: " + p.getHP());
							dlm.addElement("Attack: " + p.getAttack());
							dlm.addElement("Defense: " + p.getDefense());
							dlm.addElement("SpAtk: " + p.getSpAtk());
							dlm.addElement("SpDef: " + p.getSpDef());
							dlm.addElement("Speed: " + p.getSpeed());
							dlm.addElement("Generation: " + p.getGeneration());
							dlm.addElement("Legendary: " + p.getLegendary());
							dlm.addElement("Experience: " + p.getExperience());
							dlm.addElement("Height: " + p.getHeight());
							dlm.addElement("Weight: " + p.getWeight());
							dlm.addElement("Abilitie 1: " + p.getAbilitie1());
							dlm.addElement("Abilitie2: " + p.getAbilitie2());
							dlm.addElement("Abilitie3: " + p.getAbilitie3());
							dlm.addElement("Move 1: " + p.getMove1());
							dlm.addElement("Move 2: " + p.getMove2());
							dlm.addElement("Move 3: " + p.getMove3());
							dlm.addElement("Move 4: " + p.getMove4());
							dlm.addElement("Move 5: " + p.getMove5());
							dlm.addElement("Move 6: " + p.getMove6());
							dlm.addElement("Move 7: " + p.getMove7());
							
							listTipoPokemon.setModel(dlm);
							var = true;
						}
						
					}
					if(!var && !campo.isEmpty())
						JOptionPane.showMessageDialog(btnPesquisarTipoPokemon, "Tipo de Pokemon nao encontrado!", "Aviso", 0);
					
					
					
					
				}
			});
			btnPesquisarTipoPokemon.setBounds(302, 9, 114, 25);
			panel_3.add(btnPesquisarTipoPokemon);
			
			JButton btnRetornar_1 = new JButton("Retornar");
			btnRetornar_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					
					DefaultListModel dlm = new DefaultListModel();

					dlm.addElement("");
							
					listTipoPokemon.setModel(dlm);
							
					textFieldTipoPokemon.setText("");
					
					panelMenu.setVisible(true);
					panelPesquisaTipo.setVisible(false);
				
					
				}
			});
			btnRetornar_1.setBounds(567, 221, 114, 25);
			panelPesquisaTipo.add(btnRetornar_1);
			
			
			JPanel panelPesquisaNome = new JPanel();
			panelPesquisaNome.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelPesquisaNome, "name_6353525723667");
			panelPesquisaNome.setLayout(null);
			
			JButton btnNomePokemon = new JButton("Nome");
			btnNomePokemon.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelPesquisaNome.setVisible(true);
					panelListPokemon.setVisible(false);
					
				}
			});
			btnNomePokemon.setBounds(47, 85, 170, 70);
			panel_2.add(btnNomePokemon);
			
			JButton btnTipoPokemon = new JButton("Tipo");
			btnTipoPokemon.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelPesquisaTipo.setVisible(true);
					panelListPokemon.setVisible(false);
				}
			});
			btnTipoPokemon.setBounds(277, 85, 170, 70);
			panel_2.add(btnTipoPokemon);
			
			JPanel panelCadrastoTreinador = new JPanel();
			panelCadrastoTreinador.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelCadrastoTreinador, "name_631596121735");
			panelCadrastoTreinador.setLayout(null);
			
			JPanel panelListTreinadores = new JPanel();
			panelListTreinadores.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelListTreinadores, "name_19811906184613");
			panelListTreinadores.setLayout(null);
			
			
			JButton btnCadrastrarTreinador = new JButton("Cadrastar Treinador");
			btnCadrastrarTreinador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelCadrastoTreinador.setVisible(true);
					panelMenu.setVisible(false);
				}
			});

			btnCadrastrarTreinador.setBounds(94, 103, 218, 74);
			panelMenu.add(btnCadrastrarTreinador);
			
			JButton btnListarPokemons = new JButton("Listar Pokemons");
			btnListarPokemons.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelListPokemon.setVisible(true);
					panelMenu.setVisible(false);
					
				}
			});
			btnListarPokemons.setBounds(386, 103, 218, 74);
			panelMenu.add(btnListarPokemons);
			
			JButton btnListarTreinadores = new JButton("Listar Treinadores");
			btnListarTreinadores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelListTreinadores.setVisible(true);
					panelMenu.setVisible(false);
					
				}
			});
			btnListarTreinadores.setBounds(386, 256, 228, 69);
			panelMenu.add(btnListarTreinadores);
			
			JPanel panelAdicionarPokemons = new JPanel();
			panelAdicionarPokemons.setBackground(Color.BLACK);
			frmPokedex.getContentPane().add(panelAdicionarPokemons, "name_26916648641345");
			panelAdicionarPokemons.setLayout(null);
			
			JButton btnAdicionarPokemonsAo = new JButton("Adicionar Pokemons");
			btnAdicionarPokemonsAo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelAdicionarPokemons.setVisible(true);
					panelMenu.setVisible(false);
				}
			});
			btnAdicionarPokemonsAo.setBounds(84, 256, 228, 69);
			panelMenu.add(btnAdicionarPokemonsAo);
			
			JLabel lblMenuInicial = new JLabel("Menu Inicial");
			lblMenuInicial.setForeground(Color.WHITE);
			lblMenuInicial.setBounds(311, 12, 148, 62);
			panelMenu.add(lblMenuInicial);
			
			
			JPanel panelCampoTreinador = new JPanel();
			panelCampoTreinador.setBackground(Color.DARK_GRAY);
			panelCampoTreinador.setBounds(164, 93, 421, 153);
			panelCadrastoTreinador.add(panelCampoTreinador);
			panelCampoTreinador.setLayout(null);
			
			JLabel lblCadrastoDeTreinador = new JLabel("Cadrasto de Treinador");
			lblCadrastoDeTreinador.setForeground(Color.WHITE);
			lblCadrastoDeTreinador.setBounds(288, 33, 166, 27);
			panelCadrastoTreinador.add(lblCadrastoDeTreinador);
			
			
			JLabel lblNomeTreinador = new JLabel("Nome:");
			lblNomeTreinador.setForeground(Color.WHITE);
			lblNomeTreinador.setBounds(12, 27, 63, 25);
			panelCampoTreinador.add(lblNomeTreinador);
			
			textFieldTreinador = new JTextField();
			textFieldTreinador.setBounds(66, 26, 343, 27);
			panelCampoTreinador.add(textFieldTreinador);
			textFieldTreinador.setColumns(15);
			
			JButton btnRegistrarTreinador = new JButton("Registrar");
			btnRegistrarTreinador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					String campo = textFieldTreinador.getText();
					Treinador t = new Treinador();
					
					
					if(!campo.isEmpty())
					{
						JOptionPane.showMessageDialog(btnRegistrarTreinador, "Treinador Cadrastado!", "Aviso", 0);
						
						t = new Treinador(campo);
						treinador.add(t);
						nTreinador++;
	
					}
					else
						JOptionPane.showMessageDialog(btnRegistrarTreinador, "Entrada Inválida", "Aviso",  0);
					
					textFieldTreinador.setText("");
					
					panelMenu.setVisible(true);
					panelCadrastoTreinador.setVisible(false);
				}
				
			});
			btnRegistrarTreinador.setBounds(144, 97, 114, 25);
			panelCampoTreinador.add(btnRegistrarTreinador);
			
			
			//Listar Treinador
			
			
			JPanel panel = new JPanel();
			panel.setBackground(Color.DARK_GRAY);
			panel.setBounds(144, 51, 476, 43);
			panelListTreinadores.add(panel);
			panel.setLayout(null);
			
			JLabel lblNomeDoTreinador = new JLabel("Treinador:");
			lblNomeDoTreinador.setForeground(Color.WHITE);
			lblNomeDoTreinador.setBounds(12, 13, 114, 21);
			panel.add(lblNomeDoTreinador);
			
			textFieldPokemonsTreinador = new JTextField();
			textFieldPokemonsTreinador.setBounds(92, 13, 229, 21);
			panel.add(textFieldPokemonsTreinador);
			textFieldPokemonsTreinador.setColumns(10);
			
			JPanel panel_1 = new JPanel();
			panel_1.setBackground(Color.DARK_GRAY);
			panel_1.setBounds(144, 106, 476, 333);
			panelListTreinadores.add(panel_1);
			panel_1.setLayout(null);
			
			JLabel lblPokemons = new JLabel("Pokemons");
			lblPokemons.setForeground(Color.WHITE);
			lblPokemons.setBounds(208, 12, 70, 15);
			panel_1.add(lblPokemons);
			
			JButton btnRetornar = new JButton("Retornar");
			btnRetornar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					panelListTreinadores.setVisible(false);
					panelMenu.setVisible(true);
					
				}
			});
			btnRetornar.setBounds(208, 265, 109, 25);
			panel_1.add(btnRetornar);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(160, 39, 185, 187);
			panel_1.add(scrollPane);
			
			JList listTreinador = new JList();
			scrollPane.setViewportView(listTreinador);
			
			
			JButton btnPesquisarTreinador = new JButton("Pesquisar");
			btnPesquisarTreinador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					DefaultListModel dlm = new DefaultListModel();
					String campo = textFieldPokemonsTreinador.getText();
					boolean var = false;

					if(campo.isEmpty())
					{
						JOptionPane.showMessageDialog(btnPesquisarTreinador, "Entrada Invalida!", "Aviso", 0);
					}
				
					//Há um problema na adição de pokemons ao treinador devido uma excessão NULL POINTER gerada, oque implicou na não amostragem dos pokemons de cada treinador
					for(Treinador t: treinador)
					{
				
						if(campo.equalsIgnoreCase(t.getName()))
						{
							dlm.addElement("Treinador: " + t.getName());
							
							//for(Pokemon p: t.getPokedex())
							{
								//dlm.addElement(t.getPokedex());
							}
							
							
							var = true;
						}
						
					}
					
					listTreinador.setModel(dlm);
					
					if(!var && !campo.isEmpty())
						JOptionPane.showMessageDialog(btnPesquisarTipoPokemon, "Treinador não encontrado!", "Aviso", 0);
					
						
				}
			});
			btnPesquisarTreinador.setBounds(338, 11, 114, 25);
			panel.add(btnPesquisarTreinador);
			
			JLabel lblPesquisaDePokemons = new JLabel("Pesquisa de Pokemons de cada Treinador");
			lblPesquisaDePokemons.setForeground(Color.WHITE);
			lblPesquisaDePokemons.setBackground(Color.WHITE);
			lblPesquisaDePokemons.setBounds(231, 12, 324, 27);
			panelListTreinadores.add(lblPesquisaDePokemons);
			
			
			
			JLabel lblPesquisaNomePokemon = new JLabel("Pesquisa Nome Pokemon");
			lblPesquisaNomePokemon.setForeground(Color.WHITE);
			lblPesquisaNomePokemon.setBounds(261, 19, 190, 15);
			panelPesquisaNome.add(lblPesquisaNomePokemon);
			
			JPanel InsercaoNome = new JPanel();
			InsercaoNome.setForeground(Color.WHITE);
			InsercaoNome.setBackground(Color.DARK_GRAY);
			InsercaoNome.setBounds(153, 46, 428, 43);
			panelPesquisaNome.add(InsercaoNome);
			InsercaoNome.setLayout(null);
			
			JLabel lblNewLabel = new JLabel("Insira:");
			lblNewLabel.setForeground(Color.WHITE);
			lblNewLabel.setBounds(12, 14, 66, 15);
			InsercaoNome.add(lblNewLabel);
			
			textFieldNomePokemon = new JTextField();
			textFieldNomePokemon.setBounds(69, 12, 231, 19);
			InsercaoNome.add(textFieldNomePokemon);
			textFieldNomePokemon.setColumns(10);
			

			
			JScrollPane scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(195, 101, 335, 357);
			panelPesquisaNome.add(scrollPane_1);
			
			JList listNomePokemon = new JList();
			scrollPane_1.setViewportView(listNomePokemon);
			
			JButton btnPesquisarNomePokemon = new JButton("Pesquisar");
			btnPesquisarNomePokemon.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					String campo = textFieldNomePokemon.getText();
					DefaultListModel dlm = new DefaultListModel();
					boolean var = false;
					
					
					if(campo.isEmpty())
					{
						JOptionPane.showMessageDialog(btnRegistrarTreinador, "Entrada Invalida!", "Aviso", 0);
					}
					
					for(Pokemon p : pokemon)
					{
				
						if(campo.equalsIgnoreCase(p.getName()))
						{
							dlm.addElement("Name: " + p.getName());
							dlm.addElement("Typo 1: " + p.getType1());
							dlm.addElement("Typo 2: " + p.getType2());
							dlm.addElement("Total: " + p.getTotal());
							dlm.addElement("HP: " + p.getHP());
							dlm.addElement("Attack: " + p.getAttack());
							dlm.addElement("Defense: " + p.getDefense());
							dlm.addElement("SpAtk: " + p.getSpAtk());
							dlm.addElement("SpDef: " + p.getSpDef());
							dlm.addElement("Speed: " + p.getSpeed());
							dlm.addElement("Generation: " + p.getGeneration());
							dlm.addElement("Legendary: " + p.getLegendary());
							dlm.addElement("Experience: " + p.getExperience());
							dlm.addElement("Height: " + p.getHeight());
							dlm.addElement("Weight: " + p.getWeight());
							dlm.addElement("Abilitie 1: " + p.getAbilitie1());
							dlm.addElement("Abilitie2: " + p.getAbilitie2());
							dlm.addElement("Abilitie3: " + p.getAbilitie3());
							dlm.addElement("Move 1: " + p.getMove1());
							dlm.addElement("Move 2: " + p.getMove2());
							dlm.addElement("Move 3: " + p.getMove3());
							dlm.addElement("Move 4: " + p.getMove4());
							dlm.addElement("Move 5: " + p.getMove5());
							dlm.addElement("Move 6: " + p.getMove6());
							dlm.addElement("Move 7: " + p.getMove7());
							
							listNomePokemon.setModel(dlm);
							var = true;
						}
						
					}
					if(!var && !campo.isEmpty())
						JOptionPane.showMessageDialog(btnRegistrarTreinador, "Pokemon nao encontrado!", "Aviso", 0);
					
				}
			});
			btnPesquisarNomePokemon.setBounds(302, 9, 114, 25);
			InsercaoNome.add(btnPesquisarNomePokemon);
			
			
			JButton button = new JButton("Retornar");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					DefaultListModel dlm = new DefaultListModel();

					dlm.addElement("");
							
					listNomePokemon.setModel(dlm);
							
					textFieldNomePokemon.setText("");
					
					panelMenu.setVisible(false);
					panelPesquisaNome.setVisible(false);
					
				}
			});
			button.setBounds(585, 242, 114, 25);
			panelPesquisaNome.add(button);
			
			
			
			JLabel label_2 = new JLabel("Adição de Pokemons");
			label_2.setForeground(Color.WHITE);
			label_2.setBounds(300, 31, 140, 15);
			panelAdicionarPokemons.add(label_2);
			
			JPanel panel_7 = new JPanel();
			panel_7.setLayout(null);
			panel_7.setBackground(Color.DARK_GRAY);
			panel_7.setBounds(153, 116, 452, 165);
			panelAdicionarPokemons.add(panel_7);
			
			JLabel label_3 = new JLabel("Pokemon:");
			label_3.setForeground(Color.WHITE);
			label_3.setBounds(12, 77, 100, 26);
			panel_7.add(label_3);
			
			textFieldPokemonPesquisa = new JTextField();
			textFieldPokemonPesquisa.setColumns(10);
			textFieldPokemonPesquisa.setBounds(90, 75, 350, 26);
			panel_7.add(textFieldPokemonPesquisa);
			
			JButton AdicionarPokemonToTreinador = new JButton("Adicionar");
			AdicionarPokemonToTreinador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					String campo = textFieldTreinadorPesquisa.getText();
					boolean var = false;
					
					String campo1 = textFieldPokemonPesquisa.getText();
					boolean var1 = false;
					
					for(Treinador t : treinador)
					{
				
						if(campo.equalsIgnoreCase(t.getName()))
						{
							textFieldTreinadorPesquisa.setText("");
							var = true;
							
								//Nesse trecho de código foi constatado uma excessão NUL POINTER, o qual não fui de capaz de corrigir. Motivo que impediu a associação dos pokemons aos treinadores
								for(Pokemon p: pokemon )
								{
									if(campo1.equalsIgnoreCase(p.getName()))
									{
										var1 = true;
										//t.addPokemon(p);
										textFieldPokemonPesquisa.setText("");
									}
								}
							
						};
					}
					
					
					
					if(campo.isEmpty() || campo1.isEmpty())
					{
						JOptionPane.showMessageDialog(AdicionarPokemonToTreinador, "Entrada Invalida!", "Aviso", 0);
					}
					else
					{
						if(var && var1)
							JOptionPane.showMessageDialog(AdicionarPokemonToTreinador, "Pokemon Adicionado!", "Aviso", 0);
						
						if(!var1 && !campo1.isEmpty())
							JOptionPane.showMessageDialog(AdicionarPokemonToTreinador, "Pokemon nao encontrado!", "Aviso", 0);
						
						if(!var && !campo.isEmpty())
							JOptionPane.showMessageDialog(AdicionarPokemonToTreinador, "Treinador nao encontrado!", "Aviso", 0);
					}
					
				}
			});
			AdicionarPokemonToTreinador.setBounds(113, 128, 114, 25);
			panel_7.add(AdicionarPokemonToTreinador);
			
			JButton button_4 = new JButton("Retornar");
			button_4.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					
					panelMenu.setVisible(true);
					panelAdicionarPokemons.setVisible(false);
				}
			});
			button_4.setBounds(275, 128, 114, 25);
			panel_7.add(button_4);
			
			JLabel label_1 = new JLabel("Treinador:");
			label_1.setBounds(12, 38, 71, 15);
			panel_7.add(label_1);
			label_1.setForeground(Color.WHITE);
			
			textFieldTreinadorPesquisa = new JTextField();
			textFieldTreinadorPesquisa.setBounds(90, 36, 350, 27);
			panel_7.add(textFieldTreinadorPesquisa);
			textFieldTreinadorPesquisa.setColumns(10);
			
		}
	}
}
