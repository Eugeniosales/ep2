# Exercício Programado 2

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps/eps_2018_2/ep2/wikis/home).

## Funcionalidades do projeto


*1)* No Menu Inicial são apresentadas 4 opções:

	- Cadrasto de Treinador: Será requisitado o nome do Treinador que em seguida será armazenado em um ArrayList.
	
	- Adição de Pokemons: Uma vez informado o nome de treinador e o pokemon que será adicionado, o pokemon entrará em um ArrayList respectivo ao treinador informado. Caso o nome do treinador ou o pokemon não seja encontrado, será prontamente exibido uma menssagem na tela.
	
	- Listagem de Pokemons Nome/Tipo: Em ambos locais de pesquisa uma vez informado o nome d pokemon serão exibidos suas respectivas informações e no caso da pesquisa por tipo, serão exibidos os pokemons bem como suas características pertencentes ao tipo pesquisado.
	
	- Listagem de Treinadores: Será exibido, uma vez informado o nome de treinador, os pokemons pertencentes a Pokedex do mesmo.

## Bugs e problemas

* Houve um problema na inclusão de pokemons a pokedex dos treinadores... Foi exibido o erro: "Excessão NULLPOINTER", o qual não consegui corrigir, no entanto a estrutura de como foi pensado está coerente com o que deveria ser proposto. Isso implicou em más funcionalidade tanto na adição de Pokemons ao treinador, bem como a listagem dos treinadores.

* Os trechos que houveram problema estão comentados para vizualização do que tinha proposto 

## Referências
