package Controller;
import Model.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.FileNotFoundException;

public class BancoDeDados {
	
	public static String[][] Read(String caminho)
	{
		String [][]bancoPokemons = new String[721][20];
		
		try {
			FileInputStream arquivo = new FileInputStream(caminho);
			InputStreamReader input = new InputStreamReader(arquivo); 
			BufferedReader buffer = new BufferedReader(input); 
			String linha;
			int i = 0;
			
			
			try {
				
				linha = buffer.readLine(); //Para mover o cursor para a segunda linha
				do {
					linha = buffer.readLine();
					if(linha != null) {
							
						String[] palavras = linha.split(",");
						
						for(int j = 0; j < palavras.length; j++)
						{
							bancoPokemons[i][j] = palavras[j];
						}
						i++;
					}
				}while(linha != null);
			
			arquivo.close();
			
			}catch(Exception e) {
				System.out.println("Erro: Não foi possível ler o arquivo");
			}
		}
		catch(FileNotFoundException e) {
			System.out.println("Erro: Arquivo nao encontrado");
			
		}

		return bancoPokemons;	
	}
	
	public static String[][] concatenaArquivos(String arq_1[][], String arq_2[][])
	{
	
		String [][]bancoPokemons = new String[721][28];
		
		for(int i = 0; i < 721; i++) {
			for(int j = 0; j < 28; j++ )
			{
				if(j < 13)
					bancoPokemons[i][j] = arq_1[i][j];
				else
					bancoPokemons[i][j] = arq_2[i][j - 11];
			}
		}
		
		
		return bancoPokemons;
	}
	
	public static ArrayList<Pokemon> sets(String[][] bancoPokemon)
	{
		ArrayList <Pokemon> p= new ArrayList<Pokemon>();
		Pokemon []pokemon = new Pokemon[721];
		
		for(int i = 0; i < 721; i++)
		{
			
			pokemon[i] = new Pokemon(bancoPokemon[i][0], bancoPokemon[i][1], bancoPokemon[i][2], bancoPokemon[i][3], bancoPokemon[i][4], bancoPokemon[i][5], bancoPokemon[i][6], bancoPokemon[i][7], bancoPokemon[i][8], bancoPokemon[i][9], bancoPokemon[i][10], bancoPokemon[i][11], bancoPokemon[i][12], bancoPokemon[i][13], bancoPokemon[i][14], bancoPokemon[i][15], bancoPokemon[i][16], bancoPokemon[i][17], bancoPokemon[i][18], bancoPokemon[i][19], bancoPokemon[i][20], bancoPokemon[i][21], bancoPokemon[i][22], bancoPokemon[i][23], bancoPokemon[i][24], bancoPokemon[i][25]);
			p.add(pokemon[i]);
			
		}
		
		return p;
		
	}
	
	
	
	

}
