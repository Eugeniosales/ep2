package Model;

public class Pokemon extends Criatura
{
	private String id;
	private String Type1;
	private String Type2;
	private String Total;
	private String HP;
	private String Attack;
	private String Defense;
	private String SpAtk;
	private String SpDef;
	private String Speed;
	private String Generation;
	private String Legendary;
	private String Experience;
	private String Abilitie1;
	private String Abilitie2;
	private String Abilitie3;
	private String Move1;
	private String Move2;
	private String Move3;
	private String Move4;
	private String Move5;
	private String Move6;
	private String Move7;
	
	
	public Pokemon() {
		super();
		
	}
	
	//Polimorfismo: Sobrecarg de método, onde ambos os construtores foram utilizados.
	public Pokemon(String id, String name, String type1, String type2, String total, String hP, String attack, String defense,
			String spAtk, String spDef, String speed, String generation, String legendary, String experience, String height, String weight, 
			String abilitie1, String abilitie2, String abilitie3, String move1, String move2, String move3,
			String move4, String move5, String move6, String move7) {
		super();
		this.id = id;
		setName(name);
		Type1 = type1;
		Type2 = type2;
		Total = total;
		HP = hP;
		Attack = attack;
		Defense = defense;
		SpAtk = spAtk;
		SpDef = spDef;
		Speed = speed;
		Generation = generation;
		Legendary = legendary;
		Experience = experience;
		setHeight(height);
		setWeight(weight);
		Abilitie1 = abilitie1;
		Abilitie2 = abilitie2;
		Abilitie3 = abilitie3;
		Move1 = move1;
		Move2 = move2;
		Move3 = move3;
		Move4 = move4;
		Move5 = move5;
		Move6 = move6;
		Move7 = move7;
	}



	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getType1() {
		return Type1;
	}
	public void setType1(String type1) {
		Type1 = type1;
	}
	public String getType2() {
		return Type2;
	}
	public void setType2(String type2) {
		Type2 = type2;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getHP() {
		return HP;
	}
	public void setHP(String hP) {
		HP = hP;
	}
	public String getAttack() {
		return Attack;
	}
	public void setAttack(String attack) {
		Attack = attack;
	}
	public String getDefense() {
		return Defense;
	}
	public void setDefense(String defense) {
		Defense = defense;
	}
	public String getSpAtk() {
		return SpAtk;
	}
	public void setSpAtk(String spAtk) {
		SpAtk = spAtk;
	}
	public String getSpDef() {
		return SpDef;
	}
	public void setSpDef(String spDef) {
		SpDef = spDef;
	}
	public String getSpeed() {
		return Speed;
	}
	public void setSpeed(String speed) {
		Speed = speed;
	}
	public String getGeneration() {
		return Generation;
	}
	public void setGeneration(String generation) {
		Generation = generation;
	}
	public String getLegendary() {
		return Legendary;
	}
	public void setLegendary(String legendary) {
		Legendary = legendary;
	}
	public String getExperience() {
		return Experience;
	}
	public void setExperience(String experience) {
		Experience = experience;
	}
	public String getAbilitie1() {
		return Abilitie1;
	}
	public void setAbilitie1(String abilitie1) {
		Abilitie1 = abilitie1;
	}
	public String getAbilitie2() {
		return Abilitie2;
	}
	public void setAbilitie2(String abilitie2) {
		Abilitie2 = abilitie2;
	}
	public String getAbilitie3() {
		return Abilitie3;
	}
	public void setAbilitie3(String abilitie3) {
		Abilitie3 = abilitie3;
	}
	public String getMove1() {
		return Move1;
	}
	public void setMove1(String move1) {
		Move1 = move1;
	}
	public String getMove2() {
		return Move2;
	}
	public void setMove2(String move2) {
		Move2 = move2;
	}
	public String getMove3() {
		return Move3;
	}
	public void setMove3(String move3) {
		Move3 = move3;
	}
	public String getMove4() {
		return Move4;
	}
	public void setMove4(String move4) {
		Move4 = move4;
	}
	public String getMove5() {
		return Move5;
	}
	public void setMove5(String move5) {
		Move5 = move5;
	}
	public String getMove6() {
		return Move6;
	}
	public void setMove6(String move6) {
		Move6 = move6;
	}
	public String getMove7() {
		return Move7;
	}
	public void setMove7(String move7) {
		Move7 = move7;
	}
	
	
}
