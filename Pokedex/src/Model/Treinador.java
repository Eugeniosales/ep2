package Model;

import java.util.ArrayList;

public class Treinador extends Criatura
{
  ArrayList <Pokemon> pokedex;
  

  public Treinador() {
	super();
	
	pokedex = new ArrayList<Pokemon>();
}
  
  public Treinador(String nome)
  {
	  this.setName(nome);
  }
  
  public ArrayList<Pokemon> getPokedex() {
	return this.pokedex;
}


  public void setPokedex(ArrayList<Pokemon> pokedex) {
	this.pokedex = pokedex;
}
	
  public void addPokemon(Pokemon F) {
	  this.pokedex.add(F);	
  }
 
}

